section "Entry", rom0[$100]
entry:
    nop
    jp main

rept $150 - $104
    db 0
endr

section "Game code", rom0[$150]
main:
        ; Turn off the LCD
.waitVBlank
    ld a, [rLY]
    cp 144 ; Check if the LCD is past VBlank
    jr c, .waitVBlank

    xor a ; ld a, 0 ; We only need to reset a value with bit 7 reset, but 0 does the job
    ld [rLCDC], a ; We will have to write to LCDC again later, so it's not a bother, really.

