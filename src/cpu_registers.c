//
// Created by schnavid on 7/26/20.
//

#include "cpu_registers.h"

cpu_registers_t registers_new() {
    cpu_registers_t new;
    new.a = new.f = new.b = new.c = new.d = new.e = new.h = new.l = 0;
    new.pc = new.sp = 0;

    return new;
}

uint8_t *select_register(cpu_registers_t *registers, cpu_register8 reg) {
    switch (reg) {
    case REG_A:
        return &registers->a;
    case REG_B:
        return &registers->b;
    case REG_C:
        return &registers->c;
    case REG_D:
        return &registers->d;
    case REG_E:
        return &registers->e;
    case REG_H:
        return &registers->h;
    case REG_L:
        return &registers->l;
    default:
        return 0;
    }
}

void load_register8(cpu_registers_t *registers, cpu_register8 dst, cpu_register8 src) {
    uint8_t *dst_p = select_register(registers, dst), *src_p = select_register(registers, src);

    *dst_p = *src_p;
}

uint16_t get_register16(cpu_registers_t *registers, cpu_register16 reg) {
    switch (reg) {
    case REG_AF:
        return (((uint16_t)registers->a) << 8) + registers->f;
    case REG_BC:
        return (((uint16_t)registers->b) << 8) + registers->c;
    case REG_DE:
        return (((uint16_t)registers->d) << 8) + registers->e;
    case REG_HL:
        return (((uint16_t)registers->h) << 8) + registers->l;
    case REG_SP:
        return registers->sp;
    case REG_PC:
        return registers->pc;
    default:
        return 0;
    }
}

void set_register16(cpu_registers_t *registers, cpu_register16 reg, uint16_t data) {
    uint8_t h = (data & 0xFF00) >> 8;
    uint8_t l = (data & 0x00FF);

    switch (reg) {
    case REG_AF:
        registers->a = h;
        registers->f = l;
        break;
    case REG_BC:
        registers->b = h;
        registers->c = l;
        break;
    case REG_DE:
        registers->d = h;
        registers->e = l;
        break;
    case REG_HL:
        registers->h = h;
        registers->l = l;
        break;
    case REG_SP:
        registers->sp = data;
        break;
    case REG_PC:
        registers->pc = data;
        break;
    }
}

void set_z_flag(cpu_registers_t *registers, uint8_t flag) {
    if (flag) {
        registers->f |= Z_FLAG_MASK;
    } else {
        registers->f &= ~Z_FLAG_MASK;
    }
}

void set_n_flag(cpu_registers_t *registers, uint8_t flag) {
    if (flag) {
        registers->f |= N_FLAG_MASK;
    } else {
        registers->f &= ~N_FLAG_MASK;
    }
}

void set_h_flag(cpu_registers_t *registers, uint8_t flag) {
    if (flag) {
        registers->f |= H_FLAG_MASK;
    } else {
        registers->f &= ~H_FLAG_MASK;
    }
}

void set_cy_flag(cpu_registers_t *registers, uint8_t flag) {
    if (flag) {
        registers->f |= CY_FLAG_MASK;
    } else {
        registers->f &= ~CY_FLAG_MASK;
    }
}

uint8_t get_z_flag(cpu_registers_t *registers) {
    return registers->f & Z_FLAG_MASK;
}

uint8_t get_n_flag(cpu_registers_t *registers) {
    return registers->f & N_FLAG_MASK;
}

uint8_t get_h_flag(cpu_registers_t *registers) {
    return registers->f & H_FLAG_MASK;
}

uint8_t get_cy_flag(cpu_registers_t *registers) {
    return registers->f & CY_FLAG_MASK;
}

uint8_t get_register8(cpu_registers_t *registers, cpu_register8 reg) {
    return *select_register(registers, reg);
}

void inc_register16(cpu_registers_t *registers, cpu_register16 reg) {
    switch (reg) {
    case REG_AF:
        if (__builtin_add_overflow(registers->f, 1, &registers->f)) { registers->a++; }
        break;
    case REG_BC:
        if (__builtin_add_overflow(registers->c, 1, &registers->c)) { registers->b++; }
        break;
    case REG_DE:
        if (__builtin_add_overflow(registers->e, 1, &registers->e)) { registers->d++; }
        break;
    case REG_HL:
        if (__builtin_add_overflow(registers->l, 1, &registers->l)) { registers->h++; }
        break;
    case REG_SP:
        registers->sp++;
        break;
    case REG_PC:
        registers->pc++;
        break;
    }
}

void dec_register16(cpu_registers_t *registers, cpu_register16 reg) {
    switch (reg) {
    case REG_AF:
        if (__builtin_sub_overflow(registers->f, 1, &registers->f)) { registers->a--; }
        break;
    case REG_BC:
        if (__builtin_sub_overflow(registers->c, 1, &registers->c)) { registers->b--; }
        break;
    case REG_DE:
        if (__builtin_sub_overflow(registers->e, 1, &registers->e)) { registers->d--; }
        break;
    case REG_HL:
        if (__builtin_sub_overflow(registers->l, 1, &registers->l)) { registers->h--; }
        break;
    case REG_SP:
        registers->sp--;
        break;
    case REG_PC:
        registers->pc--;
        break;
    }
}