//
// Created by schnavid on 7/26/20.
//

#ifndef CGBE_CPU_H
#define CGBE_CPU_H

#include "cpu_registers.h"
#include <stdbool.h>
#include <stdio.h>

typedef struct cpu cpu;

cpu *cpu_new();

cpu_registers_t *cpu_registers(cpu *cpu);

uint8_t cpu_read_ram8(cpu *cpu, uint16_t addr);
void    cpu_write_ram8(cpu *cpu, uint16_t addr, uint8_t data);

void cpu_load_bootrom(cpu *cpu, FILE *bootrom);
void cpu_load_nintendo_logo(cpu *cpu);

bool cpu_stopped(cpu *cpu);
void cpu_stop(cpu *cpu);
void cpu_enable_interrupts(cpu *cpu);
void cpu_disable_interrupts(cpu *cpu);

#endif // CGBE_CPU_H
