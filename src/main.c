#include "cpu.h"
#include "cpu_instructions.h"
#include <SDL.h>
#include <stdio.h>
#include <time.h>

#define GB_CLOCK_HZ            (4194304 / 4)
#define GB_MILLI_SEC_PER_CLOCK (1000 / GB_CLOCK_HZ)
#define GB_MILLI_SEC_PER_VSYNC (1000 / 60)

static SDL_Window *  window;
static SDL_Renderer *renderer;

int sdl_init() {
    if (SDL_Init(SDL_INIT_VIDEO)) {
        printf("SDL_Init Error: %s\n", SDL_GetError());
        return 1;
    }

    SDL_Window *win = SDL_CreateWindow(
        "cgbe", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, 160, 140, SDL_WINDOW_SHOWN);
    if (!win) {
        printf("SDL_CreateWindow Error: %s\n", SDL_GetError());
        return 1;
    }

    window = win;

    SDL_Renderer *ren =
        SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);
    if (!ren) {
        SDL_DestroyWindow(window);
        printf("SDL_CreateRenderer Error: %s\n", SDL_GetError());
        return 1;
    }

    renderer = ren;

    return 0;
}

void sdl_quit() {
    SDL_DestroyRenderer(renderer);
    SDL_DestroyWindow(window);
    SDL_Quit();
}

int main() {
    if (sdl_init()) { return 1; }

    FILE *bootrom   = fopen("./test_rom/DMG_ROM.bin", "rb+");
    FILE *cartridge = fopen("./test_rom/pkmnblue.gb", "rb+");

    cpu *cpu = cpu_new();

    cpu_load_bootrom(cpu, bootrom);
    cpu_load_nintendo_logo(cpu);

    ulong     previous_millis = clock() * 1000 / CLOCKS_PER_SEC, render_acc = 0;
    uint16_t  m_clock = 0, t_clock = 0;
    SDL_Event e;
    while (!cpu_stopped(cpu)) {
        while (SDL_PollEvent(&e) != 0) {
            switch (e.type) {
            case SDL_QUIT:
                cpu_stop(cpu);
                break;
            }
        }

        ulong current_millis = clock() * 1000 / CLOCKS_PER_SEC;
        ulong elapsed_millis = current_millis - previous_millis;
        previous_millis      = current_millis;
        render_acc += elapsed_millis;

        if (cpu_registers(cpu)->pc == 0x100) { break; }
        uint8_t instruction = cpu_read_ram8(cpu, cpu_registers(cpu)->pc);
        uint8_t m           = decode_instruction(cpu, instruction);
        m_clock += m;
        t_clock += m * 4;

        while (render_acc >= GB_MILLI_SEC_PER_VSYNC) {
            SDL_SetRenderDrawColor(renderer, 10, 10, 14, SDL_ALPHA_OPAQUE);
            SDL_RenderClear(renderer);

            SDL_RenderPresent(renderer);

            render_acc -= GB_MILLI_SEC_PER_VSYNC;
        }

        SDL_Delay(elapsed_millis - GB_MILLI_SEC_PER_CLOCK);
    }

    fclose(bootrom);
    fclose(cartridge);

    sdl_quit();
    return 0;
}
