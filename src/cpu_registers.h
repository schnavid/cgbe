//
// Created by schnavid on 7/26/20.
//

#ifndef CGBE_CPU_REGISTERS_H
#define CGBE_CPU_REGISTERS_H

#include <stdint.h>

typedef struct {
    uint8_t  a, f, b, c, d, e, h, l;
    uint16_t sp, pc;
} cpu_registers_t;

typedef enum {
    REG_A = 0b111,
    REG_B = 0b000,
    REG_C = 0b001,
    REG_D = 0b010,
    REG_E = 0b011,
    REG_H = 0b100,
    REG_L = 0b101,
} cpu_register8;

typedef enum {
    REG_AF,
    REG_BC,
    REG_DE,
    REG_HL,
    REG_SP,
    REG_PC,
} cpu_register16;

cpu_registers_t registers_new();
uint8_t *       select_register(cpu_registers_t *registers, cpu_register8 reg);
void            load_register8(cpu_registers_t *registers, cpu_register8 dst, cpu_register8 src);
uint8_t         get_register8(cpu_registers_t *registers, cpu_register8 reg);
void            set_register16(cpu_registers_t *registers, cpu_register16 reg, uint16_t data);
void            inc_register16(cpu_registers_t *registers, cpu_register16 reg);
void            dec_register16(cpu_registers_t *registers, cpu_register16 reg);
uint16_t        get_register16(cpu_registers_t *registers, cpu_register16 reg);

#define Z_FLAG_MASK  0b10000000
#define N_FLAG_MASK  0b01000000
#define H_FLAG_MASK  0b00100000
#define CY_FLAG_MASK 0b00010000

void set_z_flag(cpu_registers_t *registers, uint8_t flag);
void set_n_flag(cpu_registers_t *registers, uint8_t flag);
void set_h_flag(cpu_registers_t *registers, uint8_t flag);
void set_cy_flag(cpu_registers_t *registers, uint8_t flag);

uint8_t get_z_flag(cpu_registers_t *registers);
uint8_t get_n_flag(cpu_registers_t *registers);
uint8_t get_h_flag(cpu_registers_t *registers);
uint8_t get_cy_flag(cpu_registers_t *registers);

typedef enum {
    COND_NZ = 0b00,
    COND_Z  = 0b01,
    COND_NC = 0b10,
    COND_C  = 0b11,
} condition;

#endif // CGBE_CPU_REGISTERS_H
