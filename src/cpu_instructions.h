//
// Created by schnavid on 7/26/20.
//

#ifndef CGBE_CPU_INSTRUCTIONS_H
#define CGBE_CPU_INSTRUCTIONS_H

#include "cpu.h"

uint8_t decode_instruction(cpu *cpu, uint8_t instruction);

#endif // CGBE_CPU_INSTRUCTIONS_H
