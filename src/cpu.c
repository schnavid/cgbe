//
// Created by schnavid on 7/26/20.
//

#include <stdlib.h>
#include <string.h>

#include "cpu.h"
#include "cpu_registers.h"

struct cpu {
    cpu_registers_t registers;
    uint8_t *       ram;
    uint8_t *       bootrom;
    bool            stopped;
    bool            ime;
    bool            bootrom_active;
};

cpu_registers_t *cpu_registers(cpu *cpu) {
    return &cpu->registers;
}

uint8_t cpu_read_ram8(cpu *cpu, uint16_t addr) {
    switch (addr & 0xF000) {
    // BIOS (256b)/ROM0
    case 0x0000:
        if (MMU._inbios) {
            if (addr < 0x0100)
                return MMU._bios[addr];
            else if (Z80._r.pc == 0x0100)
                MMU._inbios = 0;
        }

        return MMU._rom[addr];

        // ROM0
    case 0x1000:
    case 0x2000:
    case 0x3000:
        return MMU._rom[addr];

        // ROM1 (unbanked) (16k)
    case 0x4000:
    case 0x5000:
    case 0x6000:
    case 0x7000:
        return MMU._rom[addr];

        // Graphics: VRAM (8k)
    case 0x8000:
    case 0x9000:
        return GPU._vram[addr & 0x1FFF];

        // External RAM (8k)
    case 0xA000:
    case 0xB000:
        return MMU._eram[addr & 0x1FFF];

        // Working RAM (8k)
    case 0xC000:
    case 0xD000:
        return MMU._wram[addr & 0x1FFF];

        // Working RAM shadow
    case 0xE000:
        return MMU._wram[addr & 0x1FFF];

        // Working RAM shadow, I/O, Zero-page RAM
    case 0xF000:
        switch (addr & 0x0F00) {
        // Working RAM shadow
        case 0x000:
        case 0x100:
        case 0x200:
        case 0x300:
        case 0x400:
        case 0x500:
        case 0x600:
        case 0x700:
        case 0x800:
        case 0x900:
        case 0xA00:
        case 0xB00:
        case 0xC00:
        case 0xD00:
            return MMU._wram[addr & 0x1FFF];

            // Graphics: object attribute memory
            // OAM is 160 bytes, remaining bytes read as 0
        case 0xE00:
            if (addr < 0xFEA0)
                return GPU._oam[addr & 0xFF];
            else
                return 0;

            // Zero-page
        case 0xF00:
            if (addr >= 0xFF80) {
                return MMU._zram[addr & 0x7F];
            } else {
                // I/O control handling
                // Currently unhandled
                return 0;
            }
        }
    }
    return cpu->ram[addr];
}

void cpu_write_ram8(cpu *cpu, uint16_t addr, uint8_t data) {
    cpu->ram[addr] = data;
}

cpu *cpu_new() {
    cpu *cpu       = malloc(sizeof(struct cpu));
    cpu->registers = registers_new();
    cpu->ram       = malloc(0x10000);
    cpu->stopped   = false;

    return cpu;
}
void cpu_load_bootrom(cpu *cpu, FILE *bootrom) {
    cpu->bootrom = malloc(0x100);
    fread((void *)cpu->bootrom, sizeof(uint8_t), 0x100, bootrom);
    cpu->bootrom_active = true;
}

bool cpu_stopped(cpu *cpu) {
    return cpu->stopped;
}

void cpu_stop(cpu *cpu) {
    cpu->stopped = true;
}

void cpu_enable_interrupts(cpu *cpu) {
    cpu->ime = true;
}

void cpu_disable_interrupts(cpu *cpu) {
    cpu->ime = false;
}

void cpu_load_nintendo_logo(cpu *cpu) {
    uint8_t logo[] = {0xCE, 0xED, 0x66, 0x66, 0xCC, 0x0D, 0x00, 0x0B, 0x03, 0x73, 0x00, 0x83,
                      0x00, 0x0C, 0x00, 0x0D, 0x00, 0x08, 0x11, 0x1F, 0x88, 0x89, 0x00, 0x0E,
                      0xDC, 0xCC, 0x6E, 0xE6, 0xDD, 0xDD, 0xD9, 0x99, 0xBB, 0xBB, 0x67, 0x63,
                      0x6E, 0x0E, 0xEC, 0xCC, 0xDD, 0xDC, 0x99, 0x9F, 0xBB, 0xB9, 0x33, 0x3E};

    memcpy(&cpu->ram[0x104], logo, 48);
    cpu->ram[0x100] = 0x10;
}
