//
// Created by schnavid on 7/26/20.
//

#include "cpu_instructions.h"
#include "cpu.h"
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>

uint8_t nop_fn(cpu *_cpu, void *_data) {
    return 1;
}

/// Arithmetic and Logical Utilities
uint8_t add8(cpu *cpu, uint8_t left, uint8_t right);
uint8_t adc8(cpu *cpu, uint8_t left, uint8_t right);
uint8_t sub8(cpu *cpu, uint8_t left, uint8_t right);
uint8_t sbc8(cpu *cpu, uint8_t left, uint8_t right);
uint8_t and8(cpu *cpu, uint8_t left, uint8_t right);
uint8_t or8(cpu *cpu, uint8_t left, uint8_t right);
uint8_t xor8(cpu *cpu, uint8_t left, uint8_t right);

/// 8-Bit Transfer and Input/Output Instructions
uint8_t ld_r_r(cpu *cpu, void *data);
uint8_t ld_r_n(cpu *cpu, void *data);
uint8_t ld_r_hl(cpu *cpu, void *data);
uint8_t ld_hl_r(cpu *cpu, void *data);
uint8_t ld_hl_n(cpu *cpu, void *data);
uint8_t ld_a_dd(cpu *cpu, void *data);
uint8_t ld_a_c(cpu *cpu, void *data);
uint8_t ld_c_a(cpu *cpu, void *data);
uint8_t ld_a_n(cpu *cpu, void *data);
uint8_t ld_n_a(cpu *cpu, void *data);
uint8_t ld_a_nn(cpu *cpu, void *data);
uint8_t ld_nn_a(cpu *cpu, void *data);
uint8_t ld_a_hli(cpu *cpu, void *data);
uint8_t ld_a_hld(cpu *cpu, void *data);
uint8_t ld_dd_a(cpu *cpu, void *data);
uint8_t ld_hli_a(cpu *cpu, void *data);
uint8_t ld_hld_a(cpu *cpu, void *data);

/// 16-Bit Transfer Instructions
uint8_t ld_dd_nn(cpu *cpu, void *data);
uint8_t ld_sp_hl(cpu *cpu, void *data);
uint8_t push_qq(cpu *cpu, void *data);
uint8_t pop_qq(cpu *cpu, void *data);
uint8_t ldhl_sp_e(cpu *cpu, void *data);
uint8_t ld_nn_sp(cpu *cpu, void *data);

/// 8-Bit Arithmetic and Logical Operation Instructions
uint8_t add_a_r(cpu *cpu, void *data);
uint8_t add_a_n(cpu *cpu, void *data);
uint8_t add_a_hl(cpu *cpu, void *data);
uint8_t adc_a_r(cpu *cpu, void *data);
uint8_t adc_a_n(cpu *cpu, void *data);
uint8_t adc_a_hl(cpu *cpu, void *data);
uint8_t sub_r(cpu *cpu, void *data);
uint8_t sub_n(cpu *cpu, void *data);
uint8_t sub_hl(cpu *cpu, void *data);
uint8_t sbc_a_r(cpu *cpu, void *data);
uint8_t sbc_a_n(cpu *cpu, void *data);
uint8_t sbc_a_hl(cpu *cpu, void *data);
uint8_t and_r(cpu *cpu, void *data);
uint8_t and_n(cpu *cpu, void *data);
uint8_t and_hl(cpu *cpu, void *data);
uint8_t xor_r(cpu *cpu, void *data);
uint8_t xor_n(cpu *cpu, void *data);
uint8_t xor_hl(cpu *cpu, void *data);
uint8_t or_r(cpu *cpu, void *data);
uint8_t or_n(cpu *cpu, void *data);
uint8_t or_hl(cpu *cpu, void *data);
uint8_t cp_r(cpu *cpu, void *data);
uint8_t cp_n(cpu *cpu, void *data);
uint8_t cp_hl(cpu *cpu, void *data);
uint8_t inc_r(cpu *cpu, void *data);
uint8_t inc_hl(cpu *cpu, void *data);
uint8_t dec_r(cpu *cpu, void *data);
uint8_t dec_hl(cpu *cpu, void *data);

/// 16-Bit Arithmetic Operation Instructions
uint8_t add_hl_ss(cpu *cpu, void *data);
uint8_t add_sp_e(cpu *cpu, void *data);
uint8_t inc_ss(cpu *cpu, void *data);
uint8_t dec_ss(cpu *cpu, void *data);

/// Rotate Shift Instructions
uint8_t rla(cpu *cpu, void *data);
uint8_t rra(cpu *cpu, void *data);
uint8_t rlca(cpu *cpu, void *data);
uint8_t rrca(cpu *cpu, void *data);
uint8_t rl_r(cpu *cpu, void *data);
uint8_t rr_r(cpu *cpu, void *data);
uint8_t rlc_r(cpu *cpu, void *data);
uint8_t rrc_r(cpu *cpu, void *data);

/// Bit Operations
uint8_t bit_b_r(cpu *cpu, uint8_t b, cpu_register8 r);
uint8_t bit_b_hl(cpu *cpu, uint8_t b);
uint8_t set_b_r(cpu *cpu, uint8_t b, cpu_register8 r);
uint8_t set_b_hl(cpu *cpu, uint8_t b);
uint8_t res_b_r(cpu *cpu, uint8_t b, cpu_register8 r);
uint8_t res_b_hl(cpu *cpu, uint8_t b);

/// Jump Instructions
uint8_t jp_nn(cpu *cpu, void *data);
uint8_t jp_cc_nn(cpu *cpu, void *data);
uint8_t jr_e(cpu *cpu, void *data);
uint8_t jr_cc_e(cpu *cpu, void *data);
uint8_t jp_hl(cpu *cpu, void *data);

/// Call and Return Instructions
uint8_t call_nn(cpu *cpu, void *data);
uint8_t call_cc_nn(cpu *cpu, void *data);

/// General-Purpose Arithmetic Operations and CPU Control Instructions
uint8_t prefix_cb(cpu *cpu, void *data);
uint8_t cpl(cpu *cpu, void *data);
uint8_t stop(cpu *cpu, void *data);
uint8_t ccf(cpu *cpu, void *data);
uint8_t scf(cpu *cpu, void *data);
uint8_t ei(cpu *cpu, void *data);
uint8_t di(cpu *cpu, void *data);

struct instruction {
    uint8_t (*function)(cpu *, void *);
    void *data;
};

uint8_t add8(cpu *cpu, uint8_t left, uint8_t right) {
    uint8_t result;
    bool    carry = __builtin_add_overflow(left, right, &result);

    set_z_flag(cpu_registers(cpu), result == 0);
    set_n_flag(cpu_registers(cpu), false);
    set_h_flag(cpu_registers(cpu), (left & 0x0F) + (right & 0x0F) > 0x0F);
    set_cy_flag(cpu_registers(cpu), carry);

    return result;
}

uint8_t adc8(cpu *cpu, uint8_t left, uint8_t right) {
    uint8_t carry  = get_cy_flag(cpu_registers(cpu)) > 0;
    uint8_t result = left + right + carry;

    set_z_flag(cpu_registers(cpu), result == 0);
    set_n_flag(cpu_registers(cpu), false);
    set_h_flag(cpu_registers(cpu), (left & 0x0F) + (right & 0x0F) + carry > 0x0F);
    set_cy_flag(cpu_registers(cpu), (uint16_t)left + (uint16_t)right + (uint16_t)carry > 0xFF);

    return result;
}

uint8_t sub8(cpu *cpu, uint8_t left, uint8_t right) {
    uint8_t result;
    bool    carry = __builtin_sub_overflow(left, right, &result);

    set_z_flag(cpu_registers(cpu), result == 0);
    set_n_flag(cpu_registers(cpu), true);
    set_h_flag(cpu_registers(cpu), (((left & 0x0F) - (right & 0x0F)) & 0x10) > 0);
    set_cy_flag(cpu_registers(cpu), carry);

    return result;
}

uint8_t sbc8(cpu *cpu, uint8_t left, uint8_t right) {
    uint8_t carry  = get_cy_flag(cpu_registers(cpu)) > 0;
    uint8_t result = left - right - carry;

    set_z_flag(cpu_registers(cpu), result == 0);
    set_n_flag(cpu_registers(cpu), true);
    set_h_flag(cpu_registers(cpu), (((left & 0x0F) - (right & 0x0F) - carry) & 0x10) > 0);
    set_cy_flag(cpu_registers(cpu), left < right + carry);

    return result;
}

uint8_t and8(cpu *cpu, uint8_t left, uint8_t right) {
    uint8_t result = left & right;

    set_z_flag(cpu_registers(cpu), result == 0);
    set_n_flag(cpu_registers(cpu), false);
    set_h_flag(cpu_registers(cpu), true);
    set_cy_flag(cpu_registers(cpu), false);

    return result;
}

uint8_t or8(cpu *cpu, uint8_t left, uint8_t right) {
    uint8_t result = left | right;

    set_z_flag(cpu_registers(cpu), result == 0);
    set_n_flag(cpu_registers(cpu), false);
    set_h_flag(cpu_registers(cpu), false);
    set_cy_flag(cpu_registers(cpu), false);

    return result;
}

uint8_t xor8(cpu *cpu, uint8_t left, uint8_t right) {
    uint8_t result = left ^ right;

    set_z_flag(cpu_registers(cpu), result == 0);
    set_n_flag(cpu_registers(cpu), false);
    set_h_flag(cpu_registers(cpu), false);
    set_cy_flag(cpu_registers(cpu), false);

    return result;
}

uint8_t ld_r_r(cpu *cpu, void *data) {
    cpu_register8 *registers = data;
    cpu_register8  dst       = registers[0];
    cpu_register8  src       = registers[1];
    load_register8(cpu_registers(cpu), dst, src);

    cpu_registers(cpu)->pc++;
    return 1;
}

uint8_t ld_r_n(cpu *cpu, void *data) {
    cpu_register8 dst   = *(cpu_register8 *)data;
    uint8_t *     dst_p = select_register(cpu_registers(cpu), dst);
    *dst_p              = cpu_read_ram8(cpu, ++cpu_registers(cpu)->pc);

    cpu_registers(cpu)->pc++;
    return 2;
}

uint8_t ld_r_hl(cpu *cpu, void *data) {
    cpu_register8 dst = *(cpu_register8 *)data;

    *select_register(cpu_registers(cpu), dst) =
        cpu_read_ram8(cpu, get_register16(cpu_registers(cpu), REG_HL));

    cpu_registers(cpu)->pc++;
    return 2;
}

uint8_t ld_hl_r(cpu *cpu, void *data) {
    cpu_register8 src = *(cpu_register8 *)data;

    cpu_write_ram8(
        cpu, get_register16(cpu_registers(cpu), REG_HL), get_register8(cpu_registers(cpu), src));

    cpu_registers(cpu)->pc++;
    return 2;
}

uint8_t ld_hl_n(cpu *cpu, void *data) {
    cpu_write_ram8(
        cpu,
        get_register16(cpu_registers(cpu), REG_HL),
        cpu_read_ram8(cpu, ++cpu_registers(cpu)->pc));

    cpu_registers(cpu)->pc++;
    return 3;
}

uint8_t ld_a_dd(cpu *cpu, void *data) {
    cpu_registers(cpu)->a =
        cpu_read_ram8(cpu, get_register16(cpu_registers(cpu), *(cpu_register16 *)data));

    cpu_registers(cpu)->pc++;
    return 2;
}

uint8_t ld_a_c(cpu *cpu, void *data) {
    cpu_registers(cpu)->a = cpu_read_ram8(cpu, 0xFF00 + (uint16_t)cpu_registers(cpu)->c);

    cpu_registers(cpu)->pc++;
    return 2;
}

uint8_t ld_c_a(cpu *cpu, void *data) {
    cpu_write_ram8(cpu, 0xFF00 + (uint16_t)cpu_registers(cpu)->c, cpu_registers(cpu)->a);

    cpu_registers(cpu)->pc++;
    return 2;
}

uint8_t ld_a_n(cpu *cpu, void *data) {
    cpu_registers(cpu)->a =
        cpu_read_ram8(cpu, 0xFF00 + (uint16_t)cpu_read_ram8(cpu, ++cpu_registers(cpu)->pc));

    cpu_registers(cpu)->pc++;
    return 3;
}

uint8_t ld_n_a(cpu *cpu, void *data) {
    cpu_write_ram8(
        cpu,
        0xFF00 + (uint16_t)cpu_read_ram8(cpu, ++cpu_registers(cpu)->pc),
        cpu_registers(cpu)->a);

    cpu_registers(cpu)->pc++;
    return 3;
}

uint8_t ld_a_nn(cpu *cpu, void *data) {
    uint8_t l = cpu_read_ram8(cpu, ++cpu_registers(cpu)->pc);
    uint8_t h = cpu_read_ram8(cpu, ++cpu_registers(cpu)->pc);

    uint16_t addr = ((uint16_t)h << 8) + l;

    cpu_registers(cpu)->a = cpu_read_ram8(cpu, addr);

    cpu_registers(cpu)->pc++;
    return 4;
}

uint8_t ld_nn_a(cpu *cpu, void *data) {
    uint8_t l = cpu_read_ram8(cpu, ++cpu_registers(cpu)->pc);
    uint8_t h = cpu_read_ram8(cpu, ++cpu_registers(cpu)->pc);

    uint16_t addr = ((uint16_t)h << 8) + l;

    cpu_write_ram8(cpu, addr, cpu_registers(cpu)->a);

    cpu_registers(cpu)->pc++;
    return 4;
}

uint8_t ld_a_hli(cpu *cpu, void *data) {
    cpu_registers(cpu)->a = cpu_read_ram8(cpu, get_register16(cpu_registers(cpu), REG_HL));

    inc_register16(cpu_registers(cpu), REG_HL);

    cpu_registers(cpu)->pc++;
    return 2;
}

uint8_t ld_a_hld(cpu *cpu, void *data) {
    cpu_registers(cpu)->a = cpu_read_ram8(cpu, get_register16(cpu_registers(cpu), REG_HL));

    dec_register16(cpu_registers(cpu), REG_HL);

    cpu_registers(cpu)->pc++;
    return 2;
}

uint8_t ld_dd_a(cpu *cpu, void *data) {
    cpu_write_ram8(
        cpu, get_register16(cpu_registers(cpu), *(cpu_register16 *)data), cpu_registers(cpu)->a);

    cpu_registers(cpu)->pc++;
    return 2;
}

uint8_t ld_hli_a(cpu *cpu, void *data) {
    cpu_write_ram8(cpu, get_register16(cpu_registers(cpu), REG_HL), cpu_registers(cpu)->a);

    inc_register16(cpu_registers(cpu), REG_HL);

    cpu_registers(cpu)->pc++;
    return 2;
}

uint8_t ld_hld_a(cpu *cpu, void *data) {
    cpu_write_ram8(cpu, get_register16(cpu_registers(cpu), REG_HL), cpu_registers(cpu)->a);

    dec_register16(cpu_registers(cpu), REG_HL);

    cpu_registers(cpu)->pc++;
    return 2;
}

uint8_t ld_dd_nn(cpu *cpu, void *data) {
    uint8_t l = cpu_read_ram8(cpu, ++cpu_registers(cpu)->pc);
    uint8_t h = cpu_read_ram8(cpu, ++cpu_registers(cpu)->pc);

    uint16_t nn = ((uint16_t)h << 8) + l;

    set_register16(cpu_registers(cpu), *(cpu_register16 *)data, nn);

    cpu_registers(cpu)->pc++;
    return 3;
}

uint8_t ld_sp_hl(cpu *cpu, void *data) {
    cpu_registers(cpu)->sp = get_register16(cpu_registers(cpu), REG_HL);

    cpu_registers(cpu)->pc++;
    return 2;
}

uint8_t push_qq(cpu *cpu, void *data) {
    cpu_register16 reg   = *(cpu_register16 *)data;
    uint16_t       value = get_register16(cpu_registers(cpu), reg);
    uint8_t        l     = (uint8_t)(value & 0xFF);
    uint8_t        h     = (uint8_t)((value & 0xFF00) >> 8);

    cpu_write_ram8(cpu, --cpu_registers(cpu)->sp, h);
    cpu_write_ram8(cpu, --cpu_registers(cpu)->sp, l);

    cpu_registers(cpu)->pc++;
    return 4;
}

uint8_t pop_qq(cpu *cpu, void *data) {
    cpu_register16 reg = *(cpu_register16 *)data;
    uint8_t *      l = 0, *h = 0;

    switch (reg) {
    case REG_BC:
        h = &cpu_registers(cpu)->b;
        l = &cpu_registers(cpu)->c;
        break;
    case REG_DE:
        h = &cpu_registers(cpu)->d;
        l = &cpu_registers(cpu)->e;
        break;
    case REG_HL:
        h = &cpu_registers(cpu)->h;
        l = &cpu_registers(cpu)->l;
        break;
    case REG_AF:
        h = &cpu_registers(cpu)->a;
        l = &cpu_registers(cpu)->f;
        break;
    default:
        break;
    }

    *l = cpu_read_ram8(cpu, cpu_registers(cpu)->sp++);
    *h = cpu_read_ram8(cpu, cpu_registers(cpu)->sp++);

    cpu_registers(cpu)->pc++;
    return 3;
}

uint8_t ldhl_sp_e(cpu *cpu, void *data) {
    uint16_t e = cpu_read_ram8(cpu, ++cpu_registers(cpu)->pc);

    uint16_t result = 0;
    bool     carry  = __builtin_add_overflow(cpu_registers(cpu)->sp, e, &result);
    set_register16(cpu_registers(cpu), REG_HL, result);

    set_z_flag(cpu_registers(cpu), 0);
    set_n_flag(cpu_registers(cpu), 0);
    set_h_flag(cpu_registers(cpu), (cpu_registers(cpu)->sp & 0x0FFF) + e > 0x0FFF);
    set_cy_flag(cpu_registers(cpu), carry);

    cpu_registers(cpu)->pc++;
    return 3;
}

uint8_t ld_nn_sp(cpu *cpu, void *data) {
    uint16_t l    = cpu_read_ram8(cpu, ++cpu_registers(cpu)->pc);
    uint16_t h    = cpu_read_ram8(cpu, ++cpu_registers(cpu)->pc);
    uint16_t addr = (h << 8) + l;

    cpu_write_ram8(cpu, addr, cpu_registers(cpu)->sp & 0x00FF);
    cpu_write_ram8(cpu, addr + 1, (cpu_registers(cpu)->sp & 0xFF00) >> 8);

    return 5;
}

uint8_t add_a_r(cpu *cpu, void *data) {
    cpu_register8 right = *(cpu_register8 *)data;

    uint8_t result = add8(cpu, cpu_registers(cpu)->a, get_register8(cpu_registers(cpu), right));
    cpu_registers(cpu)->a = result;

    cpu_registers(cpu)->pc++;
    return 1;
}

uint8_t add_a_n(cpu *cpu, void *data) {
    uint8_t right = cpu_read_ram8(cpu, ++cpu_registers(cpu)->pc);

    uint8_t result        = add8(cpu, cpu_registers(cpu)->a, right);
    cpu_registers(cpu)->a = result;

    cpu_registers(cpu)->pc++;
    return 2;
}

uint8_t add_a_hl(cpu *cpu, void *data) {
    uint8_t result = add8(
        cpu, cpu_registers(cpu)->a, cpu_read_ram8(cpu, get_register16(cpu_registers(cpu), REG_HL)));
    cpu_registers(cpu)->a = result;

    cpu_registers(cpu)->pc++;
    return 2;
}

uint8_t adc_a_r(cpu *cpu, void *data) {
    cpu_register8 right = *(cpu_register8 *)data;

    uint8_t result = adc8(cpu, cpu_registers(cpu)->a, get_register8(cpu_registers(cpu), right));
    cpu_registers(cpu)->a = result;

    cpu_registers(cpu)->pc++;
    return 1;
}

uint8_t adc_a_n(cpu *cpu, void *data) {
    uint8_t right = cpu_read_ram8(cpu, ++cpu_registers(cpu)->pc);

    uint8_t result        = adc8(cpu, cpu_registers(cpu)->a, right);
    cpu_registers(cpu)->a = result;

    cpu_registers(cpu)->pc++;
    return 2;
}

uint8_t adc_a_hl(cpu *cpu, void *data) {
    uint8_t result = adc8(
        cpu, cpu_registers(cpu)->a, cpu_read_ram8(cpu, get_register16(cpu_registers(cpu), REG_HL)));
    cpu_registers(cpu)->a = result;

    cpu_registers(cpu)->pc++;
    return 2;
}

uint8_t sub_r(cpu *cpu, void *data) {
    cpu_register8 right = *(cpu_register8 *)data;

    uint8_t result = sub8(cpu, cpu_registers(cpu)->a, get_register8(cpu_registers(cpu), right));
    cpu_registers(cpu)->a = result;

    cpu_registers(cpu)->pc++;
    return 1;
}

uint8_t sub_n(cpu *cpu, void *data) {
    uint8_t right = cpu_read_ram8(cpu, ++cpu_registers(cpu)->pc);

    uint8_t result        = sub8(cpu, cpu_registers(cpu)->a, right);
    cpu_registers(cpu)->a = result;

    cpu_registers(cpu)->pc++;
    return 2;
}

uint8_t sub_hl(cpu *cpu, void *data) {
    uint8_t result = sub8(
        cpu, cpu_registers(cpu)->a, cpu_read_ram8(cpu, get_register16(cpu_registers(cpu), REG_HL)));
    cpu_registers(cpu)->a = result;

    cpu_registers(cpu)->pc++;
    return 2;
}

uint8_t sbc_a_r(cpu *cpu, void *data) {
    cpu_register8 right = *(cpu_register8 *)data;

    uint8_t result = sbc8(cpu, cpu_registers(cpu)->a, get_register8(cpu_registers(cpu), right));
    cpu_registers(cpu)->a = result;

    cpu_registers(cpu)->pc++;
    return 1;
}

uint8_t sbc_a_n(cpu *cpu, void *data) {
    uint8_t right = cpu_read_ram8(cpu, ++cpu_registers(cpu)->pc);

    uint8_t result        = sbc8(cpu, cpu_registers(cpu)->a, right);
    cpu_registers(cpu)->a = result;

    cpu_registers(cpu)->pc++;
    return 2;
}

uint8_t sbc_a_hl(cpu *cpu, void *data) {
    uint8_t result = sbc8(
        cpu, cpu_registers(cpu)->a, cpu_read_ram8(cpu, get_register16(cpu_registers(cpu), REG_HL)));
    cpu_registers(cpu)->a = result;

    cpu_registers(cpu)->pc++;
    return 2;
}

uint8_t and_r(cpu *cpu, void *data) {
    cpu_register8 right = *(cpu_register8 *)data;

    uint8_t result = and8(cpu, cpu_registers(cpu)->a, get_register8(cpu_registers(cpu), right));
    cpu_registers(cpu)->a = result;

    cpu_registers(cpu)->pc++;
    return 1;
}

uint8_t and_n(cpu *cpu, void *data) {
    uint8_t right = cpu_read_ram8(cpu, ++cpu_registers(cpu)->pc);

    uint8_t result        = and8(cpu, cpu_registers(cpu)->a, right);
    cpu_registers(cpu)->a = result;

    cpu_registers(cpu)->pc++;
    return 2;
}

uint8_t and_hl(cpu *cpu, void *data) {
    uint8_t result = and8(
        cpu, cpu_registers(cpu)->a, cpu_read_ram8(cpu, get_register16(cpu_registers(cpu), REG_HL)));
    cpu_registers(cpu)->a = result;

    cpu_registers(cpu)->pc++;
    return 2;
}

uint8_t xor_r(cpu *cpu, void *data) {
    cpu_register8 right = *(cpu_register8 *)data;

    uint8_t result = xor8(cpu, cpu_registers(cpu)->a, get_register8(cpu_registers(cpu), right));
    cpu_registers(cpu)->a = result;

    cpu_registers(cpu)->pc++;
    return 1;
}

uint8_t xor_n(cpu *cpu, void *data) {
    uint8_t right = cpu_read_ram8(cpu, ++cpu_registers(cpu)->pc);

    uint8_t result        = xor8(cpu, cpu_registers(cpu)->a, right);
    cpu_registers(cpu)->a = result;

    cpu_registers(cpu)->pc++;
    return 2;
}

uint8_t xor_hl(cpu *cpu, void *data) {
    uint8_t result = xor8(
        cpu, cpu_registers(cpu)->a, cpu_read_ram8(cpu, get_register16(cpu_registers(cpu), REG_HL)));
    cpu_registers(cpu)->a = result;

    cpu_registers(cpu)->pc++;
    return 2;
}

uint8_t or_r(cpu *cpu, void *data) {
    cpu_register8 right = *(cpu_register8 *)data;

    uint8_t result = or8(cpu, cpu_registers(cpu)->a, get_register8(cpu_registers(cpu), right));
    cpu_registers(cpu)->a = result;

    cpu_registers(cpu)->pc++;
    return 1;
}

uint8_t or_n(cpu *cpu, void *data) {
    uint8_t right = cpu_read_ram8(cpu, ++cpu_registers(cpu)->pc);

    uint8_t result        = or8(cpu, cpu_registers(cpu)->a, right);
    cpu_registers(cpu)->a = result;

    cpu_registers(cpu)->pc++;
    return 2;
}

uint8_t or_hl(cpu *cpu, void *data) {
    uint8_t result = or8(
        cpu, cpu_registers(cpu)->a, cpu_read_ram8(cpu, get_register16(cpu_registers(cpu), REG_HL)));
    cpu_registers(cpu)->a = result;

    cpu_registers(cpu)->pc++;
    return 2;
}

uint8_t cp_r(cpu *cpu, void *data) {
    cpu_register8 right = *(cpu_register8 *)data;

    sub8(cpu, cpu_registers(cpu)->a, get_register8(cpu_registers(cpu), right));

    cpu_registers(cpu)->pc++;
    return 1;
}

uint8_t cp_n(cpu *cpu, void *data) {
    uint8_t right = cpu_read_ram8(cpu, ++cpu_registers(cpu)->pc);

    sub8(cpu, cpu_registers(cpu)->a, right);

    cpu_registers(cpu)->pc++;
    return 2;
}

uint8_t cp_hl(cpu *cpu, void *data) {
    sub8(
        cpu, cpu_registers(cpu)->a, cpu_read_ram8(cpu, get_register16(cpu_registers(cpu), REG_HL)));

    cpu_registers(cpu)->pc++;
    return 2;
}

uint8_t inc_r(cpu *cpu, void *data) {
    cpu_register8 reg = *(cpu_register8 *)data;

    bool carry = get_cy_flag(cpu_registers(cpu));

    *select_register(cpu_registers(cpu), reg) =
        add8(cpu, get_register8(cpu_registers(cpu), reg), 1);

    set_cy_flag(cpu_registers(cpu), carry);

    cpu_registers(cpu)->pc++;
    return 1;
}

uint8_t inc_hl(cpu *cpu, void *data) {
    bool carry = get_cy_flag(cpu_registers(cpu));

    cpu_write_ram8(
        cpu,
        get_register16(cpu_registers(cpu), REG_HL),
        add8(cpu, cpu_read_ram8(cpu, get_register16(cpu_registers(cpu), REG_HL)), 1));

    set_cy_flag(cpu_registers(cpu), carry);

    cpu_registers(cpu)->pc++;
    return 3;
}

uint8_t dec_r(cpu *cpu, void *data) {
    cpu_register8 reg = *(cpu_register8 *)data;

    bool carry = get_cy_flag(cpu_registers(cpu));

    *select_register(cpu_registers(cpu), reg) =
        sub8(cpu, get_register8(cpu_registers(cpu), reg), 1);

    set_cy_flag(cpu_registers(cpu), carry);

    cpu_registers(cpu)->pc++;
    return 1;
}

uint8_t dec_hl(cpu *cpu, void *data) {
    bool carry = get_cy_flag(cpu_registers(cpu));

    cpu_write_ram8(
        cpu,
        get_register16(cpu_registers(cpu), REG_HL),
        sub8(cpu, cpu_read_ram8(cpu, get_register16(cpu_registers(cpu), REG_HL)), 1));

    set_cy_flag(cpu_registers(cpu), carry);

    cpu_registers(cpu)->pc++;
    return 3;
}

uint8_t add_hl_ss(cpu *cpu, void *data) {
    cpu_register16 reg = *(cpu_register16 *)data;

    switch (reg) {
    case REG_BC:
        cpu_registers(cpu)->l = add8(cpu, cpu_registers(cpu)->l, cpu_registers(cpu)->c);
        cpu_registers(cpu)->h = adc8(cpu, cpu_registers(cpu)->h, cpu_registers(cpu)->b);
        break;
    case REG_DE:
        cpu_registers(cpu)->l = add8(cpu, cpu_registers(cpu)->l, cpu_registers(cpu)->e);
        cpu_registers(cpu)->h = adc8(cpu, cpu_registers(cpu)->h, cpu_registers(cpu)->d);
        break;
    case REG_HL:
        cpu_registers(cpu)->l = add8(cpu, cpu_registers(cpu)->l, cpu_registers(cpu)->l);
        cpu_registers(cpu)->h = adc8(cpu, cpu_registers(cpu)->h, cpu_registers(cpu)->h);
        break;
    case REG_SP:
        cpu_registers(cpu)->l = add8(cpu, cpu_registers(cpu)->l, cpu_registers(cpu)->sp & 0x00FF);
        cpu_registers(cpu)->h =
            adc8(cpu, cpu_registers(cpu)->h, (cpu_registers(cpu)->sp & 0xFF00) >> 8);
        break;
    default:
        break;
    }

    cpu_registers(cpu)->pc++;
    return 2;
}

uint8_t add_sp_e(cpu *cpu, void *data) {
    uint8_t e = cpu_read_ram8(cpu, ++cpu_registers(cpu)->pc);

    uint8_t l = add8(cpu, e, cpu_registers(cpu)->sp & 0x00FF);
    uint8_t h = adc8(cpu, 0, (cpu_registers(cpu)->sp & 0xFF00) >> 8);

    cpu_registers(cpu)->sp = ((uint16_t)h << 8) + l;

    cpu_registers(cpu)->pc++;
    return 4;
}

uint8_t inc_ss(cpu *cpu, void *data) {
    cpu_register16 reg = *(cpu_register16 *)data;

    inc_register16(cpu_registers(cpu), reg);

    cpu_registers(cpu)->pc++;
    return 2;
}

uint8_t dec_ss(cpu *cpu, void *data) {
    cpu_register16 reg = *(cpu_register16 *)data;

    dec_register16(cpu_registers(cpu), reg);

    cpu_registers(cpu)->pc++;
    return 2;
}

uint8_t rla(cpu *cpu, void *data) {
    set_z_flag(cpu_registers(cpu), false);
    set_n_flag(cpu_registers(cpu), false);
    set_h_flag(cpu_registers(cpu), false);
    uint8_t old_carry = get_cy_flag(cpu_registers(cpu)) > 0;
    set_cy_flag(cpu_registers(cpu), cpu_registers(cpu)->a & 0b10000000);

    cpu_registers(cpu)->a <<= 1;
    cpu_registers(cpu)->a |= old_carry;

    cpu_registers(cpu)->pc++;
    return 1;
}

uint8_t rra(cpu *cpu, void *data) {
    set_z_flag(cpu_registers(cpu), false);
    set_n_flag(cpu_registers(cpu), false);
    set_h_flag(cpu_registers(cpu), false);
    uint8_t old_carry = get_cy_flag(cpu_registers(cpu)) > 0;
    set_cy_flag(cpu_registers(cpu), cpu_registers(cpu)->a & 0b00000001);

    cpu_registers(cpu)->a >>= 1;
    cpu_registers(cpu)->a |= (old_carry << 7);

    cpu_registers(cpu)->pc++;
    return 1;
}

uint8_t rlca(cpu *cpu, void *data) {
    set_z_flag(cpu_registers(cpu), false);
    set_n_flag(cpu_registers(cpu), false);
    set_h_flag(cpu_registers(cpu), false);
    set_cy_flag(cpu_registers(cpu), cpu_registers(cpu)->a & 0x80);

    cpu_registers(cpu)->a <<= 1;
    cpu_registers(cpu)->a |= (get_cy_flag(cpu_registers(cpu)) > 0);

    cpu_registers(cpu)->pc++;
    return 1;
}

uint8_t rrca(cpu *cpu, void *data) {
    set_z_flag(cpu_registers(cpu), false);
    set_n_flag(cpu_registers(cpu), false);
    set_h_flag(cpu_registers(cpu), false);
    set_cy_flag(cpu_registers(cpu), cpu_registers(cpu)->a & 0x01);

    cpu_registers(cpu)->a >>= 1;
    cpu_registers(cpu)->a |= (get_cy_flag(cpu_registers(cpu)) > 0);

    cpu_registers(cpu)->pc++;
    return 1;
}

uint8_t jp_nn(cpu *cpu, void *data) {
    uint8_t l = cpu_read_ram8(cpu, ++cpu_registers(cpu)->pc);
    uint8_t h = cpu_read_ram8(cpu, ++cpu_registers(cpu)->pc);

    uint16_t addr = ((uint16_t)h << 8) + l;

    cpu_registers(cpu)->pc = addr;

    return 4;
}

uint8_t jp_cc_nn(cpu *cpu, void *data) {
    condition cond = *(condition *)data;

    uint8_t l = cpu_read_ram8(cpu, ++cpu_registers(cpu)->pc);
    uint8_t h = cpu_read_ram8(cpu, ++cpu_registers(cpu)->pc);

    uint16_t addr = ((uint16_t)h << 8) + l;

    switch (cond) {
    case COND_NZ:
        if (!get_z_flag(cpu_registers(cpu))) {
            cpu_registers(cpu)->pc = addr;
            return 3;
        } else {
            cpu_registers(cpu)->pc++;
            return 2;
        }
    case COND_Z:
        if (get_z_flag(cpu_registers(cpu))) {
            cpu_registers(cpu)->pc = addr;
            return 3;
        } else {
            cpu_registers(cpu)->pc++;
            return 2;
        }
    case COND_NC:
        if (!get_cy_flag(cpu_registers(cpu))) {
            cpu_registers(cpu)->pc = addr;
            return 3;
        } else {
            cpu_registers(cpu)->pc++;
            return 2;
        }
    case COND_C:
        if (get_cy_flag(cpu_registers(cpu))) {
            cpu_registers(cpu)->pc = addr;
            return 3;
        } else {
            cpu_registers(cpu)->pc++;
            return 2;
        }
    default:
        cpu_registers(cpu)->pc++;
        return 2;
    }
}

uint8_t jr_e(cpu *cpu, void *data) {
    int8_t e = (int8_t)cpu_read_ram8(cpu, ++cpu_registers(cpu)->pc) + 2;

    cpu_registers(cpu)->pc += e;
    return 3;
}

uint8_t jr_cc_e(cpu *cpu, void *data) {
    condition cond = *(condition *)data;

    int8_t e = (int8_t)cpu_read_ram8(cpu, ++cpu_registers(cpu)->pc) + 2;

    switch (cond) {
    case COND_NZ:
        if (!get_z_flag(cpu_registers(cpu))) {
            cpu_registers(cpu)->pc += e;
            return 3;
        } else {
            cpu_registers(cpu)->pc++;
            return 2;
        }
    case COND_Z:
        if (get_z_flag(cpu_registers(cpu))) {
            cpu_registers(cpu)->pc += e;
            return 3;
        } else {
            cpu_registers(cpu)->pc++;
            return 2;
        }
    case COND_NC:
        if (!get_cy_flag(cpu_registers(cpu))) {
            cpu_registers(cpu)->pc += e;
            return 3;
        } else {
            cpu_registers(cpu)->pc++;
            return 2;
        }
    case COND_C:
        if (get_cy_flag(cpu_registers(cpu))) {
            cpu_registers(cpu)->pc += e;
            return 3;
        } else {
            cpu_registers(cpu)->pc++;
            return 2;
        }
    default:
        cpu_registers(cpu)->pc++;
        return 2;
    }
}

uint8_t bit_b_r(cpu *cpu, uint8_t b, cpu_register8 r) {
    set_z_flag(cpu_registers(cpu), !(get_register8(cpu_registers(cpu), r) & (1 << b)));

    return 2;
}

uint8_t bit_b_hl(cpu *cpu, uint8_t b) {
    set_z_flag(
        cpu_registers(cpu),
        !(cpu_read_ram8(cpu, get_register16(cpu_registers(cpu), REG_HL)) & (1 << b)));

    return 2;
}

uint8_t call_nn(cpu *cpu, void *data) {
    uint8_t l = cpu_read_ram8(cpu, ++cpu_registers(cpu)->pc);
    uint8_t h = cpu_read_ram8(cpu, ++cpu_registers(cpu)->pc);

    uint16_t addr = ((uint16_t)h << 8) + l;

    cpu_registers(cpu)->pc++;
    cpu_write_ram8(cpu, --cpu_registers(cpu)->sp, (cpu_registers(cpu)->pc & 0xFF00) >> 8);
    cpu_write_ram8(cpu, --cpu_registers(cpu)->sp, cpu_registers(cpu)->pc & 0x00FF);

    cpu_registers(cpu)->pc = addr;
    return 6;
}

uint8_t call_cc_nn(cpu *cpu, void *data) {
    switch (*(condition *)data) {
    case COND_NZ:
        if (!get_n_flag(cpu_registers(cpu))) { return call_nn(cpu, 0); }
        break;
    case COND_Z:
        if (get_n_flag(cpu_registers(cpu))) { return call_nn(cpu, 0); }
        break;
    case COND_NC:
        if (!get_cy_flag(cpu_registers(cpu))) { return call_nn(cpu, 0); }
        break;
    case COND_C:
        if (get_cy_flag(cpu_registers(cpu))) { return call_nn(cpu, 0); }
        break;
    }

    cpu_registers(cpu)->pc++;
    return 3;
}

uint8_t prefix_cb(cpu *cpu, void *data) {
    uint8_t instruction = cpu_read_ram8(cpu, ++cpu_registers(cpu)->pc);

    cpu_registers(cpu)->pc++;
    switch (instruction & 0b11000000) {
    case 0b01000000:
        if ((instruction & 0b00000111) == 0b110) {
            return bit_b_hl(cpu, (instruction & 0b00111000) >> 3);
        } else {
            return bit_b_r(cpu, (instruction & 0b00111000) >> 3, instruction & 0b00000111);
        }
    }

    return 1;
}

uint8_t cpl(cpu *cpu, void *data) {
    cpu_registers(cpu)->a = ~cpu_registers(cpu)->a;

    set_n_flag(cpu_registers(cpu), true);
    set_h_flag(cpu_registers(cpu), true);

    cpu_registers(cpu)->pc++;
    return 1;
}
uint8_t stop(cpu *cpu, void *data) {
    cpu_read_ram8(cpu, ++cpu_registers(cpu)->pc);

    cpu_stop(cpu);

    return 1;
}

uint8_t ccf(cpu *cpu, void *data) {
    set_n_flag(cpu_registers(cpu), false);
    set_h_flag(cpu_registers(cpu), false);
    set_cy_flag(cpu_registers(cpu), (get_cy_flag(cpu_registers(cpu)) > 0) ^ 1);

    return 1;
}

uint8_t scf(cpu *cpu, void *data) {
    set_n_flag(cpu_registers(cpu), false);
    set_h_flag(cpu_registers(cpu), false);
    set_cy_flag(cpu_registers(cpu), true);

    return 1;
}

uint8_t ei(cpu *cpu, void *data) {
    cpu_enable_interrupts(cpu);

    return 1;
}

uint8_t di(cpu *cpu, void *data) {
    cpu_disable_interrupts(cpu);

    return 1;
}

static const struct instruction nop = {
    &nop_fn,
    0,
};

struct instruction instructions[0x100] = {
    // ############################ 0x0x ############################
    /* 0x00 */ {&nop_fn, 0},
    /* 0x01 */ {&ld_dd_nn, (cpu_register16[1]){REG_BC}},
    /* 0x02 */ {&ld_dd_a, (cpu_register16[1]){REG_BC}},
    /* 0x03 */ {&inc_ss, (cpu_register16[1]){REG_BC}},
    /* 0x04 */ {&inc_r, (cpu_register8[1]){REG_B}},
    /* 0x05 */ {&dec_r, (cpu_register8[1]){REG_B}},
    /* 0x06 */ {&ld_r_n, (cpu_register8[1]){REG_B}},
    /* 0x07 */ {&rlca, 0},
    /* 0x08 */ {&ld_nn_sp, 0},
    /* 0x09 */ {&add_hl_ss, (cpu_register16[1]){REG_BC}},
    /* 0x0A */ {&ld_a_dd, (cpu_register16[1]){REG_BC}},
    /* 0x0B */ {&dec_ss, (cpu_register16[1]){REG_BC}},
    /* 0x0C */ {&inc_r, (cpu_register8[1]){REG_C}},
    /* 0x0D */ {&dec_r, (cpu_register8[1]){REG_C}},
    /* 0x0E */ {&ld_r_n, (cpu_register8[1]){REG_C}},
    /* 0x0F */ {&rrca, 0},
    // ############################ 0x1x ############################
    /* 0x10 */ {&stop, 0},
    /* 0x11 */ {&ld_dd_nn, (cpu_register16[1]){REG_DE}},
    /* 0x12 */ {&ld_dd_a, (cpu_register16[1]){REG_DE}},
    /* 0x13 */ {&inc_ss, (cpu_register16[1]){REG_DE}},
    /* 0x14 */ {&inc_r, (cpu_register8[1]){REG_D}},
    /* 0x15 */ {&dec_r, (cpu_register8[1]){REG_D}},
    /* 0x16 */ {&ld_r_n, (cpu_register8[1]){REG_D}},
    /* 0x17 */ {&rla, 0},
    /* 0x18 */ {&jr_e, 0},
    /* 0x19 */ {&add_hl_ss, (cpu_register16[1]){REG_DE}},
    /* 0x1A */ {&ld_a_dd, (cpu_register16[1]){REG_DE}},
    /* 0x1B */ {&dec_ss, (cpu_register16[1]){REG_DE}},
    /* 0x1C */ {&inc_r, (cpu_register8[1]){REG_E}},
    /* 0x1D */ {&dec_r, (cpu_register8[1]){REG_E}},
    /* 0x1E */ {&ld_r_n, (cpu_register8[1]){REG_E}},
    /* 0x1F */ {&rra, 0},
    // ############################ 0x2x ############################
    /* 0x20 */ {&jr_cc_e, (condition[1]){COND_NZ}},
    /* 0x21 */ {&ld_dd_nn, (cpu_register16[1]){REG_HL}},
    /* 0x22 */ {&ld_hli_a, 0},
    /* 0x23 */ {&inc_ss, (cpu_register16[1]){REG_HL}},
    /* 0x24 */ {&inc_r, (cpu_register8[1]){REG_H}},
    /* 0x25 */ {&dec_r, (cpu_register8[1]){REG_H}},
    /* 0x26 */ {&ld_r_n, (cpu_register8[1]){REG_H}},
    /* 0x27 */ nop,
    /* 0x28 */ {&jr_cc_e, (condition[1]){COND_Z}},
    /* 0x29 */ {&add_hl_ss, (cpu_register16[1]){REG_HL}},
    /* 0x2A */ {&ld_a_hli, 0},
    /* 0x2B */ {&dec_ss, (cpu_register16[1]){REG_HL}},
    /* 0x2C */ {&inc_r, (cpu_register8[1]){REG_L}},
    /* 0x2D */ {&dec_r, (cpu_register8[1]){REG_L}},
    /* 0x2E */ {&ld_r_n, (cpu_register8[1]){REG_L}},
    /* 0x2F */ {&cpl, 0},
    // ############################ 0x3x ############################
    /* 0x30 */ {&jr_cc_e, (condition[1]){COND_NC}},
    /* 0x31 */ {&ld_dd_nn, (cpu_register16[1]){REG_SP}},
    /* 0x32 */ {&ld_hld_a, 0},
    /* 0x33 */ {&inc_ss, (cpu_register16[1]){REG_SP}},
    /* 0x34 */ {&inc_hl, 0},
    /* 0x35 */ {&dec_hl, 0},
    /* 0x36 */ {&ld_hl_n, 0},
    /* 0x37 */ {&scf, 0},
    /* 0x38 */ {&jr_cc_e, (condition[1]){COND_C}},
    /* 0x39 */ {&add_hl_ss, (cpu_register16[1]){REG_SP}},
    /* 0x3A */ {&ld_a_hld, 0},
    /* 0x3B */ {&inc_ss, (cpu_register16[1]){REG_SP}},
    /* 0x3C */ {&inc_r, (cpu_register8[1]){REG_A}},
    /* 0x3D */ {&dec_r, (cpu_register8[1]){REG_A}},
    /* 0x3E */ {&ld_r_n, (cpu_register8[1]){REG_A}},
    /* 0x3F */ {&ccf, 0},
    // ############################ 0x4x ############################
    /* 0x40 */ {&ld_r_r, (cpu_register8[2]){REG_B, REG_B}},
    /* 0x41 */ {&ld_r_r, (cpu_register8[2]){REG_B, REG_C}},
    /* 0x42 */ {&ld_r_r, (cpu_register8[2]){REG_B, REG_D}},
    /* 0x43 */ {&ld_r_r, (cpu_register8[2]){REG_B, REG_E}},
    /* 0x44 */ {&ld_r_r, (cpu_register8[2]){REG_B, REG_H}},
    /* 0x45 */ {&ld_r_r, (cpu_register8[2]){REG_B, REG_L}},
    /* 0x46 */ {&ld_r_hl, (cpu_register8[1]){REG_B}},
    /* 0x47 */ {&ld_r_r, (cpu_register8[2]){REG_B, REG_A}},
    /* 0x48 */ {&ld_r_r, (cpu_register8[2]){REG_C, REG_B}},
    /* 0x49 */ {&ld_r_r, (cpu_register8[2]){REG_C, REG_C}},
    /* 0x4A */ {&ld_r_r, (cpu_register8[2]){REG_C, REG_D}},
    /* 0x4B */ {&ld_r_r, (cpu_register8[2]){REG_C, REG_E}},
    /* 0x4C */ {&ld_r_r, (cpu_register8[2]){REG_C, REG_H}},
    /* 0x4D */ {&ld_r_r, (cpu_register8[2]){REG_C, REG_L}},
    /* 0x4E */ {&ld_r_hl, (cpu_register8[1]){REG_C}},
    /* 0x4F */ {&ld_r_r, (cpu_register8[2]){REG_C, REG_A}},
    // ############################ 0x5x ############################
    /* 0x50 */ {&ld_r_r, (cpu_register8[2]){REG_D, REG_B}},
    /* 0x51 */ {&ld_r_r, (cpu_register8[2]){REG_D, REG_C}},
    /* 0x52 */ {&ld_r_r, (cpu_register8[2]){REG_D, REG_D}},
    /* 0x53 */ {&ld_r_r, (cpu_register8[2]){REG_D, REG_E}},
    /* 0x54 */ {&ld_r_r, (cpu_register8[2]){REG_D, REG_H}},
    /* 0x55 */ {&ld_r_r, (cpu_register8[2]){REG_D, REG_L}},
    /* 0x56 */ {&ld_r_hl, (cpu_register8[1]){REG_D}},
    /* 0x57 */ {&ld_r_r, (cpu_register8[2]){REG_D, REG_A}},
    /* 0x58 */ {&ld_r_r, (cpu_register8[2]){REG_E, REG_B}},
    /* 0x59 */ {&ld_r_r, (cpu_register8[2]){REG_E, REG_C}},
    /* 0x5A */ {&ld_r_r, (cpu_register8[2]){REG_E, REG_D}},
    /* 0x5B */ {&ld_r_r, (cpu_register8[2]){REG_E, REG_E}},
    /* 0x5C */ {&ld_r_r, (cpu_register8[2]){REG_E, REG_H}},
    /* 0x5D */ {&ld_r_r, (cpu_register8[2]){REG_E, REG_L}},
    /* 0x5E */ {&ld_r_hl, (cpu_register8[1]){REG_E}},
    /* 0x5F */ {&ld_r_r, (cpu_register8[2]){REG_E, REG_A}},
    // ############################ 0x6x ############################
    /* 0x60 */ {&ld_r_r, (cpu_register8[2]){REG_H, REG_B}},
    /* 0x61 */ {&ld_r_r, (cpu_register8[2]){REG_H, REG_C}},
    /* 0x62 */ {&ld_r_r, (cpu_register8[2]){REG_H, REG_D}},
    /* 0x63 */ {&ld_r_r, (cpu_register8[2]){REG_H, REG_E}},
    /* 0x64 */ {&ld_r_r, (cpu_register8[2]){REG_H, REG_H}},
    /* 0x65 */ {&ld_r_r, (cpu_register8[2]){REG_H, REG_L}},
    /* 0x66 */ {&ld_r_hl, (cpu_register8[1]){REG_H}},
    /* 0x67 */ {&ld_r_r, (cpu_register8[2]){REG_L, REG_A}},
    /* 0x68 */ {&ld_r_r, (cpu_register8[2]){REG_L, REG_B}},
    /* 0x69 */ {&ld_r_r, (cpu_register8[2]){REG_L, REG_C}},
    /* 0x6A */ {&ld_r_r, (cpu_register8[2]){REG_L, REG_D}},
    /* 0x6B */ {&ld_r_r, (cpu_register8[2]){REG_L, REG_E}},
    /* 0x6C */ {&ld_r_r, (cpu_register8[2]){REG_L, REG_H}},
    /* 0x6D */ {&ld_r_r, (cpu_register8[2]){REG_L, REG_L}},
    /* 0x6E */ {&ld_r_hl, (cpu_register8[1]){REG_L}},
    /* 0x6F */ {&ld_r_r, (cpu_register8[2]){REG_L, REG_A}},
    // ############################ 0x7x ############################
    /* 0x70 */ {&ld_hl_r, (cpu_register8[1]){REG_B}},
    /* 0x71 */ {&ld_hl_r, (cpu_register8[1]){REG_C}},
    /* 0x72 */ {&ld_hl_r, (cpu_register8[1]){REG_D}},
    /* 0x73 */ {&ld_hl_r, (cpu_register8[1]){REG_E}},
    /* 0x74 */ {&ld_hl_r, (cpu_register8[1]){REG_H}},
    /* 0x75 */ {&ld_hl_r, (cpu_register8[1]){REG_L}},
    /* 0x76 */ nop,
    /* 0x77 */ {&ld_hl_r, (cpu_register8[1]){REG_A}},
    /* 0x78 */ {&ld_r_r, (cpu_register8[2]){REG_A, REG_B}},
    /* 0x79 */ {&ld_r_r, (cpu_register8[2]){REG_A, REG_C}},
    /* 0x7A */ {&ld_r_r, (cpu_register8[2]){REG_A, REG_D}},
    /* 0x7B */ {&ld_r_r, (cpu_register8[2]){REG_A, REG_E}},
    /* 0x7C */ {&ld_r_r, (cpu_register8[2]){REG_A, REG_H}},
    /* 0x7D */ {&ld_r_r, (cpu_register8[2]){REG_A, REG_L}},
    /* 0x7E */ {&ld_r_hl, (cpu_register8[1]){REG_A}},
    /* 0x7F */ {&ld_r_r, (cpu_register8[2]){REG_A, REG_A}},
    // ############################ 0x8x ############################
    /* 0x80 */ {&add_a_r, (cpu_register8[1]){REG_B}},
    /* 0x81 */ {&add_a_r, (cpu_register8[1]){REG_C}},
    /* 0x82 */ {&add_a_r, (cpu_register8[1]){REG_D}},
    /* 0x83 */ {&add_a_r, (cpu_register8[1]){REG_E}},
    /* 0x84 */ {&add_a_r, (cpu_register8[1]){REG_H}},
    /* 0x85 */ {&add_a_r, (cpu_register8[1]){REG_L}},
    /* 0x86 */ {&add_a_hl, 0},
    /* 0x87 */ {&add_a_r, (cpu_register8[1]){REG_A}},
    /* 0x88 */ {&adc_a_r, (cpu_register8[1]){REG_B}},
    /* 0x89 */ {&adc_a_r, (cpu_register8[1]){REG_C}},
    /* 0x8A */ {&adc_a_r, (cpu_register8[1]){REG_D}},
    /* 0x8B */ {&adc_a_r, (cpu_register8[1]){REG_E}},
    /* 0x8C */ {&adc_a_r, (cpu_register8[1]){REG_H}},
    /* 0x8D */ {&adc_a_r, (cpu_register8[1]){REG_L}},
    /* 0x8E */ {&adc_a_hl, 0},
    /* 0x8F */ {&adc_a_r, (cpu_register8[1]){REG_A}},
    // ############################ 0x9x ############################
    /* 0x90 */ {&sub_r, (cpu_register8[1]){REG_B}},
    /* 0x91 */ {&sub_r, (cpu_register8[1]){REG_C}},
    /* 0x92 */ {&sub_r, (cpu_register8[1]){REG_D}},
    /* 0x93 */ {&sub_r, (cpu_register8[1]){REG_E}},
    /* 0x94 */ {&sub_r, (cpu_register8[1]){REG_H}},
    /* 0x95 */ {&sub_r, (cpu_register8[1]){REG_L}},
    /* 0x96 */ {&sub_hl, 0},
    /* 0x97 */ {&sub_r, (cpu_register8[1]){REG_A}},
    /* 0x98 */ {&sbc_a_r, (cpu_register8[1]){REG_B}},
    /* 0x99 */ {&sbc_a_r, (cpu_register8[1]){REG_C}},
    /* 0x9A */ {&sbc_a_r, (cpu_register8[1]){REG_D}},
    /* 0x9B */ {&sbc_a_r, (cpu_register8[1]){REG_E}},
    /* 0x9C */ {&sbc_a_r, (cpu_register8[1]){REG_H}},
    /* 0x9D */ {&sbc_a_r, (cpu_register8[1]){REG_L}},
    /* 0x9E */ {&sbc_a_hl, 0},
    /* 0x9F */ {&sbc_a_r, (cpu_register8[1]){REG_A}},
    // ############################ 0xAx ############################
    /* 0xA0 */ {&and_r, (cpu_register8[1]){REG_B}},
    /* 0xA1 */ {&and_r, (cpu_register8[1]){REG_C}},
    /* 0xA2 */ {&and_r, (cpu_register8[1]){REG_D}},
    /* 0xA3 */ {&and_r, (cpu_register8[1]){REG_E}},
    /* 0xA4 */ {&and_r, (cpu_register8[1]){REG_H}},
    /* 0xA5 */ {&and_r, (cpu_register8[1]){REG_L}},
    /* 0xA6 */ {&and_hl, 0},
    /* 0xA7 */ {&and_r, (cpu_register8[1]){REG_A}},
    /* 0xA8 */ {&xor_r, (cpu_register8[1]){REG_B}},
    /* 0xA9 */ {&xor_r, (cpu_register8[1]){REG_C}},
    /* 0xAA */ {&xor_r, (cpu_register8[1]){REG_D}},
    /* 0xAB */ {&xor_r, (cpu_register8[1]){REG_E}},
    /* 0xAC */ {&xor_r, (cpu_register8[1]){REG_H}},
    /* 0xAD */ {&xor_r, (cpu_register8[1]){REG_L}},
    /* 0xAE */ {&xor_hl, 0},
    /* 0xAF */ {&xor_r, (cpu_register8[1]){REG_A}},
    // ########################### 0xBx ############################
    /* 0xB0 */ {&or_r, (cpu_register8[1]){REG_B}},
    /* 0xB1 */ {&or_r, (cpu_register8[1]){REG_C}},
    /* 0xB2 */ {&or_r, (cpu_register8[1]){REG_D}},
    /* 0xB3 */ {&or_r, (cpu_register8[1]){REG_E}},
    /* 0xB4 */ {&or_r, (cpu_register8[1]){REG_H}},
    /* 0xB5 */ {&or_r, (cpu_register8[1]){REG_L}},
    /* 0xB6 */ {&or_hl, 0},
    /* 0xB7 */ {&or_r, (cpu_register8[1]){REG_A}},
    /* 0xB8 */ {&cp_r, (cpu_register8[1]){REG_B}},
    /* 0xB9 */ {&cp_r, (cpu_register8[1]){REG_C}},
    /* 0xBA */ {&cp_r, (cpu_register8[1]){REG_D}},
    /* 0xBB */ {&cp_r, (cpu_register8[1]){REG_E}},
    /* 0xBC */ {&cp_r, (cpu_register8[1]){REG_H}},
    /* 0xBD */ {&cp_r, (cpu_register8[1]){REG_L}},
    /* 0xBE */ {&cp_hl, 0},
    /* 0xBF */ {&cp_r, (cpu_register8[1]){REG_A}},
    // ############################ 0xCx ############################
    /* 0xC0 */ nop,
    /* 0xC1 */ {&pop_qq, (cpu_register16[1]){REG_BC}},
    /* 0xC2 */ {&jp_cc_nn, (condition[1]){COND_NZ}},
    /* 0xC3 */ {&jp_nn, 0},
    /* 0xC4 */ {&call_cc_nn, (condition[1]){COND_NZ}},
    /* 0xC5 */ {&push_qq, (cpu_register16[1]){REG_BC}},
    /* 0xC6 */ {&add_a_n, 0},
    /* 0xC7 */ nop,
    /* 0xC8 */ nop,
    /* 0xC9 */ nop,
    /* 0xCA */ {&jp_cc_nn, (condition[1]){COND_Z}},
    /* 0xCB */ {&prefix_cb, 0},
    /* 0xCC */ {&call_cc_nn, (condition[1]){COND_Z}},
    /* 0xCD */ {&call_nn, 0},
    /* 0xCE */ {&adc_a_n, 0},
    /* 0xCF */ nop,
    // ############################ 0xDx ############################
    /* 0xD0 */ nop,
    /* 0xD1 */ {&pop_qq, (cpu_register16[1]){REG_DE}},
    /* 0xD2 */ {&jp_cc_nn, (condition[1]){COND_NC}},
    /* 0xD3 */ {&nop_fn, 0},
    /* 0xD4 */ {&call_cc_nn, (condition[1]){COND_NC}},
    /* 0xD5 */ {&push_qq, (cpu_register16[1]){REG_DE}},
    /* 0xD6 */ {&sub_n, 0},
    /* 0xD7 */ nop,
    /* 0xD8 */ nop,
    /* 0xD9 */ nop,
    /* 0xDA */ {&jp_cc_nn, (condition[1]){COND_C}},
    /* 0xDB */ {&nop_fn, 0},
    /* 0xDC */ {&call_cc_nn, (condition[1]){COND_C}},
    /* 0xDD */ {&nop_fn, 0},
    /* 0xDE */ {&sbc_a_n, 0},
    /* 0xDF */ nop,
    // ############################ 0xEx ############################
    /* 0xE0 */ {&ld_n_a, 0},
    /* 0xE1 */ {&pop_qq, (cpu_register16[1]){REG_HL}},
    /* 0xE2 */ {&ld_c_a, 0},
    /* 0xE3 */ {&nop_fn, 0},
    /* 0xE4 */ {&nop_fn, 0},
    /* 0xE5 */ {&push_qq, (cpu_register16[1]){REG_HL}},
    /* 0xE6 */ {&and_n, 0},
    /* 0xE7 */ nop,
    /* 0xE8 */ {&add_sp_e, 0},
    /* 0xE9 */ nop,
    /* 0xEA */ {&ld_nn_a, 0},
    /* 0xEB */ {&nop_fn, 0},
    /* 0xEC */ {&nop_fn, 0},
    /* 0xED */ {&nop_fn, 0},
    /* 0xEE */ {&xor_n, 0},
    /* 0xEF */ nop,
    // ############################ 0xFx ############################
    /* 0xF0 */ {&ld_a_n, 0},
    /* 0xF1 */ {&pop_qq, (cpu_register16[1]){REG_AF}},
    /* 0xF2 */ {&ld_a_c, 0},
    /* 0xF3 */ {&di, 0},
    /* 0xF4 */ {&nop_fn, 0},
    /* 0xF5 */ {&push_qq, (cpu_register16[1]){REG_AF}},
    /* 0xF6 */ {&or_n, 0},
    /* 0xF7 */ nop,
    /* 0xF8 */ {&ldhl_sp_e, 0},
    /* 0xF9 */ {&ld_sp_hl, 0},
    /* 0xFA */ {&ld_a_nn, 0},
    /* 0xFB */ {&ei, 0},
    /* 0xFC */ {&nop_fn, 0},
    /* 0xFD */ {&nop_fn, 0},
    /* 0xFE */ {&cp_n, 0},
    /* 0xFF */ nop,
};

uint8_t decode_instruction(cpu *cpu, uint8_t instruction) {
    struct instruction instr = instructions[instruction];
    return (instr.function)(cpu, instr.data);
}
